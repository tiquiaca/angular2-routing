import { ModuleWithProviders}  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgencyListComponent } from './agency/agency-list.component';
import { ClaimsListComponent } from './claims/claims-list.component';
import { HomeComponent} from './home/home.component';

//Route configuration
export const routes: Routes= [
    {path: '', component:HomeComponent},
    {path: 'agency', component:AgencyListComponent},
    {path: 'claims', component:ClaimsListComponent}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);



