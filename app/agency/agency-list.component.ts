
import { Component } from '@angular/core';

@Component({
  template: `<br/>
  <div class="col-md-2"></div>
    <div class="col-md-8">
    <div class="row"> 
    <div class="panel panel-default">
    <div class="panel-heading">Agency list</div>
    <!-- List group -->
        <ul class="list-group">
          <li class="list-group-item" *ngFor="let agency of agencies">{{agency}}</li>
        </ul>
      </div>
    </div>
  </div>
<div class="col-md-2"></div>
    `
})

// Component class
export class AgencyListComponent {
  agencies =['Agency 1','Agency 2','Agency 3','Agency 4','Agency 5','Agency 6','Agency 7','Agency 8','Agency 9'];
}