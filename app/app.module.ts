import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';
import { HttpModule, JsonpModule} from  '@angular/http';

import { AppComponent }  from './app.component';
import { routing } from './app.routes';
import { AgencyListComponent } from './agency/agency-list.component';
import { ClaimsListComponent } from './claims/claims-list.component';
import { HomeComponent } from './home/home.component';


@NgModule({
  imports:      [ BrowserModule, FormsModule, HttpModule, JsonpModule, routing ],
  declarations: [ AppComponent,  HomeComponent, AgencyListComponent, ClaimsListComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
