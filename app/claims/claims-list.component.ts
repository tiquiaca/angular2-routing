
import { Component } from '@angular/core';

@Component({
  template: `
    <br/>
  <div class="col-md-2"></div>
    <div class="col-md-8">
    <div class="row"> 
    <div class="panel panel-primary">
    <div class="panel-heading">Claims list</div>
    <!-- List group -->
        <ul class="list-group">
          <li class="list-group-item" *ngFor="let claim of claims">{{claim}}</  li>
        </ul>
      </div>
    </div>
  </div>
<div class="col-md-2"></div>
    `
})

// Component class
export class ClaimsListComponent {
  claims =['Claims 1','Claims 2','Claims 3','Claims 4','Claims 5','Claims 6','Claims 7','Claims 8','Claims 9'];
}