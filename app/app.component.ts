import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  <div class="jumbotron">
    <h1>Angular 2</h1>
    <p>Component routing sample</p>
 </div>
 <div>
    <ul class="nav nav-tabs">
      <li role="presentation" class="active">
        <a [routerLink]="['/']">Home</a>
      </li>
      <li role="presentation">
        <a [routerLink]="['/agency']">Agency</a>
      </li>
      <li role="presentation">
        <a [routerLink]="['/claims']">Claims</a>
      </li>
    </ul>
  </div>

   <!-- Router Outlet -->
   <div>
    <router-outlet></router-outlet>
   </div>
    
  `,
})
export class AppComponent  {}
